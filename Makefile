BINARY_NAME=main.out
 
build:
	go build -o ${BINARY_NAME} main.go
 
run:
	./${BINARY_NAME}
 
clean:
	go clean
	rm ${BINARY_NAME}
